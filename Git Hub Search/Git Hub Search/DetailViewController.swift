//
//  DetailViewController.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 10/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UIViewController {
    
    var selectedRepo: RootNode!
    var repoDetails: [Repository]!
    var requestHandler: RequestHandler?
    var dataManager: DataManager?
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        repoDetails = selectedRepo.repositories
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50.0
        
        // Set the title
        //        navigationItem.title = selectedRepo.nme
    }
    
    // MARK: - Table View Data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    //     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    //
    //        switch section {
    //        case 0:
    //            return repoDetails.count
    //        case 1:
    //            return dataManager?.data(for: selectedRepo).count ?? 0 //.readMe(for: selectedRepo).count ?? 0
    //        default:
    //            return 0
    //        }
    //    }
    //     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //
    //            // For the section displying the user details
    //            switch indexPath.section {
    //            case 0:
    //
    //                // Dequeue the cell to display the user details
    //                let cell = tableView.dequeueReusableCell(withIdentifier: "RepoDetailCell", for: indexPath)
    //
    //                // Decompose the tuple
    //                let (type, value) = repoDetails?[indexPath.row]
    //
    //                // Set the values using the tuple
    //                cell.detailTextLabel?.text = type
    //                cell.textLabel?.text = value
    //
    //                // Clear the ImageView incase we reuse it
    //                cell.imageView?.image = nil
    //
    //                // For the first cell in the section we want to display the user avatar
    //                if indexPath.row == 0 {
    //
    //                    // Fetch the avatar
    //                    // NOTE: This shound always hit the cache as we fetching the same image again
    //                    requestHandler?.fetchAdorableAvatar(selectedUser.username) { image in
    //
    //                        // UI Update = Main Queue
    //                        DispatchQueue.main.async {
    //                            cell.imageView?.image = image
    //                            cell.setNeedsLayout()
    //                        }
    //                    }
    //                }
    //
    //                return cell
    //
    //                //
    //            case 1:
    //                let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath)
    //
    //                guard let posts = dataManager?.posts(for: selectedRepo),
    //                    let comments = dataManager?.comments(for: posts[indexPath.row]) else { return cell }
    //
    //                cell.textLabel?.text = posts[indexPath.row].title
    //                cell.detailTextLabel?.text = "\(comments.count)"
    //
    //                return cell
    //            default: return UITableViewCell()
    //            }
    //        }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0: return "Repo Details"
        case 1: return "Read Me"
        default: return nil
        }
        
    }
}
