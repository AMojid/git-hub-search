//
//  AccountModel.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 09/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation
import CoreData

class AccountModel {
    
    var currentAccount: Account?
    var existingAccount: Account?
    let coreDataStack: CoreDataStack
    
    private var items:[NSManagedObject] = []
    
    init(_ coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
    }
    
    func createAccount(named htmlUrlAccount: String, withAvatarUrl avatarUrl: String) {
        
        // Create the account
        let newAccount = Account(context: coreDataStack.mainContext)
        
        // Update
        newAccount.htmlUrlAccount = htmlUrlAccount
        newAccount.avatarUrl = avatarUrl
        
        // Save context
        coreDataStack.saveContext()
    }
   
    func updateAccount(named htmlUrlAccount: String, withAvatarUrl avatarUrl: String) {
        
        // Fetch the Account
        currentAccount?.htmlUrlAccount = htmlUrlAccount
        currentAccount?.avatarUrl = avatarUrl
        
        prefetch()
        
        // Update
        
        if items.count == 0 {
            
            createAccount(named: htmlUrlAccount, withAvatarUrl: avatarUrl)
            
        } else {
            
            existingAccount = items[0] as? Account
            
            guard let updatingAccount = existingAccount else { return }
            
            if updatingAccount.avatarUrl != avatarUrl {
                
                updatingAccount.avatarUrl = avatarUrl
                // Save context
                coreDataStack.saveContext()
            }
            
        }
    }
    
    func prefetch() {
        
        let allAccounts: NSFetchRequest = Account.fetchRequest()
        guard let AccountUrl = currentAccount?.htmlUrlAccount else { return }
        let onlyInSelectedAccount = NSPredicate(format: "account.htmlUrlAccount == %@", AccountUrl)
        
        allAccounts.predicate = onlyInSelectedAccount
        
        do {
            items = try coreDataStack.mainContext.fetch(allAccounts)
        } catch {
            print(error)
        }
    }
    
}

