//
//  DataManager.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 08/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//


import Foundation

struct RootNode: Codable {
    
    let totalCount: Int
    let incompleteResults: Bool
    let repositories: [Repository]
    
    private enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case repositories = "items"
    }
}

struct Repository: Codable {
    
    let shortName: String
    let fullName: String
    let longDescription: String
    let repoScore: Double
    let repoOwner: RepoOwner
    let repoLanguage: String
    let watchersCount: Int
    let forksCount: Int
    let openIssuesCount: Int
    let htmlUrlRepo: String
    
    private enum CodingKeys: String, CodingKey {
        case shortName = "name"
        case fullName = "full_name"
        case longDescription = "description"
        case repoScore = "score"
        case repoOwner = "owner"
        case repoLanguage = "language"
        case watchersCount = "watchers_count"
        case forksCount = "forks_count"
        case openIssuesCount = "open_issues_count"
        case htmlUrlRepo = "html_url"
    }
}

struct RepoOwner: Codable {
    
    let avatarUrl: String
    let htmlUrlAccount: String
    
    private enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case htmlUrlAccount = "html_url"
    }
}

class DataManager {
    
    
    let data: RootNode
    let model = AccountModel(CoreDataStack.shared)
    
    init(data: RootNode) {
        
        self.data = data
    }
    
    internal func saveData() {
        
        for (index, element) in data.repositories.enumerated() {
            
            print(data.repositories)
            
            model.updateAccount(named: element.repoOwner.htmlUrlAccount, withAvatarUrl: element.repoOwner.avatarUrl)
            
        }
        
    }
}

