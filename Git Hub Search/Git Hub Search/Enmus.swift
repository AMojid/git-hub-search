//
//  Enmus.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 09/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation

enum HEADERS {
    
    static let gitHubRestV3 = "Accept: application/vnd.github.v3+json"
}

enum GITHUB {
    
    static let scheme = "https"
    static let HTMLHost = "github.com"
    static let ApiHost = "api.github.com"
}

enum ENDPOINTS {
    
    static let readMe = "/blob/master/README.md"
    static let searchRepositories = "/search/repositories"
}

enum QUERYITEM {
    
    static let name = "in:name"
    static let language = "in:language"
    static let sortBy = "sort:interactions-desc"
}
