//
//  AppDelegate.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 08/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        CoreDataStack.shared.saveContext()
    }
    
}

