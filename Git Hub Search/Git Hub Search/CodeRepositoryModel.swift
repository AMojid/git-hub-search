//
//  CodeRepositoryModel.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 09/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation
import CoreData

class CodeRepositoryModel {
    
    var currentAccount: Account
    var currentAccountGUID: String
    let coreDataStack: CoreDataStack
    
    private var items:[NSManagedObject] = []
    
    init(coreDataStack: CoreDataStack, currentAccount: Account) {
        self.coreDataStack = coreDataStack
        self.currentAccount = currentAccount
        self.currentAccountGUID = String(currentAccount.htmlUrlAccount.hashValue)
    }
    
    func createCodeRepository(shortName: String, fullName: String, longDescription: String, repoScore: Double, repoLanguage: String, watchersCount: Int64, forksCount: Int64, openIssuesCount: Int64, htmlUrlRepo: String) {
        
        // Create the CodeRepository
        let newCodeRepository = CodeRepository(context: coreDataStack.mainContext)
        
        // Update
        newCodeRepository.accountGUID = String(currentAccount.htmlUrlAccount.hashValue)
        newCodeRepository.forks = forksCount
        newCodeRepository.fullDescription = longDescription
        newCodeRepository.htmlUrlRepository = htmlUrlRepo
        newCodeRepository.language = repoLanguage
        newCodeRepository.longName = fullName
        newCodeRepository.name = shortName
        newCodeRepository.openIssues = openIssuesCount
        newCodeRepository.score = repoScore
        newCodeRepository.watchers = watchersCount
        
        // Save context
        coreDataStack.saveContext()
    }
}

extension CodeRepositoryModel {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        
        guard section >= 0 && section < numberOfSections else {
            return 0
        }
        
        return items.count
    }
    
    func item(at indexPath: IndexPath) -> NSManagedObject? {
        
        guard indexPath.row >= 0 && indexPath.row < numberOfRows(inSection: indexPath.section) else {
            return nil
        }
        
        return items[indexPath.row]
        
    }
}
