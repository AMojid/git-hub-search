//
//  ViewController.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 08/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
// initial setup 

import UIKit
import CoreData

class BaseViewController: UIViewController, UITextFieldDelegate {
    
    // The model, we inject the strck into model
    let coreDataStack = CoreDataStack.shared
    let requestHandler = RequestHandler()
    var dataManager: DataManager!
    let models = [CodeRepository]()
    var filteredModels = [CodeRepository]()

    let cellId = "RepoCell"
    
    let searchController = UISearchController(searchResultsController: nil)
    
    
    var textFieldInsideSearchBar: UITextField!
    // The TableView
    @IBOutlet var tableView: UITableView!
    // A reference to the class which will deal with the request
    
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController <CodeRepository> = {
        let fetchRequest:NSFetchRequest = CodeRepository.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        //managedObjectContext is your instance of NSManagerdObjectContext
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStack.shared.mainContext, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    } ()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        do {
            try fetchedResultsController.performFetch()
        } catch let fetchError {
            print("Fetch Error: \(fetchError.localizedDescription)")
        }
        
        // Receipy for table view
        tableView.layer.borderWidth = 1.0
        tableView.layer.backgroundColor = UIColor.black.cgColor
        
        
        // Hide the empty cells
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        setupSearchController()
        textFieldInsideSearchBar.delegate = self
        textFieldInsideSearchBar.returnKeyType = .done
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupSearchController() {
        searchController.searchBar.placeholder = "Repo Name"
        textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
        //        searchController.searchResultsUpdater = self
        tableView.tableHeaderView = searchController.searchBar
    }
    
    
    private func fetchSearchQuery(searchTerm: String) -> Void {
        
        // Use the requestHandler to fetch the Comments
        requestHandler.fetch(searchTerm) { (jsonData) in
            guard let jsonData = jsonData else { return }
            
            JSONParser().parse(jsonData, into: RootNode.self, completion: { [unowned self] outcome in
                switch outcome {
                case .failure(let err): print(err)
                case .success(let data): DataManager(data: data).saveData()
                }
            })
        }
    }
    
    
    internal func textFieldShouldReturn(_ textFieldInsideSearchBar: UITextField) -> Bool {
        
        guard let searchText = textFieldInsideSearchBar.text else { return true }
        
        let plusSearchText = searchText.replacingOccurrences(of: " ", with: "+")
        print(plusSearchText)
        fetchSearchQuery(searchTerm: plusSearchText)
        
        textFieldInsideSearchBar.resignFirstResponder()
        textFieldInsideSearchBar.text = nil
        return true
    }
    
//        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//    
//            // Switch on the identifier
//            switch segue.identifier {
//            case UIStoryboardSegue.keys.toShowRepoDetails:
//    
//                // Safely unwrap the next view controller and the selectedIndexPath of the TableView
//                if let nextVC = segue.destination as? DetailViewController,
//                    let indexPath = tableView.indexPathForSelectedRow,
//                    let selectedRepo = dataManager. {
//    
//                    // Pass the selected repo
//                    nextVC.selectedRepo = selectedRepo
//    
//                    // Pass the requestHandler
//                    nextVC.requestHandler = requestHandler
//    
//                    // Pass the dataManager
//                    nextVC.dataManager = dataManager
//                }
//    
//            default: break
//            }
//    
//        }
    
}


extension UIStoryboardSegue {
    enum keys {
        static let toShowRepoDetails = "showRepoDetail"
    }
}

extension BaseViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredModels.count
        }
        
        return 1 //models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        //        let model: Model
        //        if searchController.isActive && searchController.searchBar.text != "" {
        //            model = filteredModels[indexPath.row]
        //        } else {
        //            model = models[indexPath.row]
        //        }
        cell.textLabel!.text = " hellow "//model.
        cell.detailTextLabel!.text = "World" //model.
        return cell
    }
    
}


extension BaseViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        @unknown default:
            fatalError("@unknown default")
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                tableView.moveRow(at: indexPath, to: newIndexPath)
            }
        @unknown default:
            fatalError("@unknown default")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
