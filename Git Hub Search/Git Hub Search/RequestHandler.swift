//
//  RequestHandler.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 09/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//



import Foundation
import UIKit.UIImage


class RequestHandler {
    
    let imageCache: NSCache<NSString, UIImage>
    
    init() { imageCache = NSCache<NSString, UIImage>() }
    
    // A function to fetch the avatars
    // The completion handler for accepting the images
    func fetchAccountAvatar(_ named: String, completion: @escaping (UIImage) -> Void) {
        
        var imageURLComponents = URLComponents()
        imageURLComponents.scheme = GITHUB.scheme
        imageURLComponents.host = GITHUB.HTMLHost
        imageURLComponents.path = "/avatars/285/\(named).png"
        
        // Create the URL from the username
        if let imageURL = imageURLComponents.url {
            
            // If the image exists in the cache, then use it
            if let image = imageCache.object(forKey: imageURL.absoluteString as NSString) {
                
                // Return the image in the closure
                print("Fetched cached image")
                completion(image)
                
            } else {
                
                // Create a URLSessionDataTask
                let dataTask = URLSession.shared.dataTask(with: imageURL) { (data, _, error) in
                    
                    if let error = error {
                        print(error.localizedDescription)
                    } else if let data = data, let image = UIImage(data: data) {
                        
                        // Save the image in the cache
                        self.imageCache.setObject(image, forKey: imageURL.absoluteString as NSString)
                        
                        //Return the image in the closure
                        print("Fetched Image from service")
                        completion(image)
                    }
                }
                
                dataTask.resume()
            }
            
        }
        
    }
    
    // A shell fetch function to get data from the server
    func fetch(_ searchTerms: String, completion: @escaping (Data?) -> Void) {
        
        let searchlanguageName = searchTerms + "+" + QUERYITEM.name + "+" + QUERYITEM.language + "+" + QUERYITEM.sortBy
        
        // Attempt to create the URL with help from the ENUM
        var urlComponents = URLComponents()
        urlComponents.scheme = GITHUB.scheme
        urlComponents.host = GITHUB.ApiHost
        urlComponents.path = ENDPOINTS.searchRepositories
        urlComponents.queryItems = [
            URLQueryItem(name: "q", value: searchlanguageName),
        ]
        
        guard let url = urlComponents.url else { return }
        
        print(url)
        
        
         let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
         
         if let error = error {
         print(error.localizedDescription)
         completion(nil)
         } else if let data = data, let response = response as? HTTPURLResponse {
         
         switch response.statusCode {
         case 200...299:
         completion(data)
         default:
         print("Invalid HTTP status code \(response.statusCode)")
         completion(nil)
         }
         
         }
         }
         
         dataTask.resume()
        
    }
}

enum Outcome<T> {
    case success(T)
    case failure(String)
}

protocol Parser {
    func parse<T>(_ data: Data, into: T.Type, completion: (Outcome<T>) -> Void) where T: Codable
}

struct JSONParser: Parser {
    
    func parse<T>(_ data: Data, into: T.Type, completion: (Outcome<T>) -> Void) where T : Decodable, T : Encodable {
        
        do {
            let result = try JSONDecoder().decode(into, from: data)
            completion(.success(result))
        } catch {
            completion(.failure(error.localizedDescription))
        }
        
    }
    
}





