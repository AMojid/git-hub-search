//
//  CoreDataStack.swift
//  Git Hub Search
//
//  Created by Abdul Mojid on 08/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation
import CoreData


// MARK: - Core Data stack
class CoreDataStack {
    
    // Some shared variable
    static let shared = CoreDataStack()
    private init() {}
    
    
    var mainContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    var privateContext: NSManagedObjectContext {
        let newConetxt = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        newConetxt.parent = mainContext
        return newConetxt
    }
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Git_Hub_Search")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    @discardableResult
    func saveContext () -> Bool {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        return false
    }
}

